package com.boxever;

import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface UserDAO {

    @SqlUpdate("INSERT INTO users " +
            "( ref,  email,  name) "
            +
            "VALUES " +
            "(:ref, :email, :name)")
    @GetGeneratedKeys
    int insert(@BindBean User user);

}
