# README #

This is a very simple sample on how to test your JDBI SQL and your flyway migration scripts. There is more details on the [How to test your JDBI/ Flyway blog post](http://www.boxever.com/using-flyway-h2-manage-test-db-migrations-queries).

### How do I get set up? ###

* Install [gradle](http://gradle.org/)
* Run `gradle build`

Edit the code in your favourite IDE